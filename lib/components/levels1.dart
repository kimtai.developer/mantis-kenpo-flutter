import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class Levels1  extends StatelessWidget {
  const Levels1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 10),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                buildLevelContainer(context,Icons.accessibility_outlined,Colors.white, Colors.grey.shade800,Colors.grey.shade800,'white','White'),
                buildLevelContainer(context,Icons.run_circle_outlined,Colors.yellow.shade600,Colors.grey.shade900,Colors.grey.shade800,'yellow','Yellow'),
                buildLevelContainer(context,Icons.gamepad_outlined,Colors.orange.shade800,Colors.white,Colors.white,'orange','Orange'),
              ],
            ),
            const SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                buildLevelContainer(context,Icons.gps_fixed_rounded,Colors.purple.shade700, Colors.white,Colors.white,'purple','Purple'),
                buildLevelContainer(context,Icons.device_hub_outlined,Colors.teal.shade700,Colors.white,Colors.white,'green','Green'),
                buildLevelContainer(context,Icons.assignment_turned_in_rounded,Colors.brown.shade800,Colors.white,Colors.white,'brown','Brown'),
              ],
            )
          ],
        ),
      ),
    );
  }

  GestureDetector buildLevelContainer(BuildContext context,IconData ico, Color color, Color icoColor, Color textColor,String link,String label) {
    return GestureDetector(
      onTap: () {
        context.pushNamed(link);
      },
      child: Container(
                width: (MediaQuery.of(context).size.width / 3) -12,
                height: (MediaQuery.of(context).size.width / 3) -12,
                decoration:  BoxDecoration(
                  color: color,
                  borderRadius: const BorderRadius.all(Radius.circular(10))
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                        ico,
                        color: icoColor,
                        size: 30.0,
                      ),
                    Text(label,style:TextStyle(fontSize: 14,color: textColor))
                  ],
                ),
              ),
    );
  }
}
