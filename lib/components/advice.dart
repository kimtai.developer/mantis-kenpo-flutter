import 'package:flutter/material.dart';

class Advice extends StatelessWidget {
  const Advice({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}

class BulletList extends StatelessWidget {
  final List<String> strings;
  const BulletList({Key? key, required this.strings}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: strings.map((str) {
          return Container(
            margin: const EdgeInsets.symmetric(vertical: 3),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Text("• ", style: TextStyle(fontWeight: FontWeight.bold),),
                const SizedBox(
                  width: 2,
                ),
                Expanded(
                  child: Text(
                    str,
                    textAlign: TextAlign.left,
                    softWrap: true,
                    style: const TextStyle(
                      fontSize: 16,
                        color: Colors.black
                    ),
                  ),
                ),
              ],
            ),
          );
        }).toList(),
      ),

    );
  }
}

