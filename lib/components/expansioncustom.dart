import 'package:flutter/material.dart';
import 'package:kenpo/components/listinfo.dart';
import 'package:kenpo/theme/theme.dart';

class CustomExpansion extends StatelessWidget {

  final String title;
  final List arg;
  const CustomExpansion({Key? key, required this.title, required this.arg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 4),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        child: ExpansionTile(
          collapsedShape: const RoundedRectangleBorder(
            side: BorderSide.none,
          ),
          shape: const RoundedRectangleBorder(
            side: BorderSide.none,
          ),
          textColor: CustomData.fontColor,
          backgroundColor: Colors.white,
          collapsedBackgroundColor: Colors.white,
          title: Text(title,style: TextStyle(
              fontWeight: FontWeight.w700,
              color: CustomData.fontColor),
          ),
          children: <Widget>[
            ListInfo(result: arg)
          ],
        ),
      ),
    );
  }
}