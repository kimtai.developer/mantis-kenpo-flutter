import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class Levels extends StatelessWidget {
  const Levels({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView(
        padding: EdgeInsets.all(10),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10
        ),
        children:  <Widget>[
          StatusPanel(label:'White Belt', panelColor: Colors.white, textColor: Colors.grey.shade900, link: 'white',),
          StatusPanel(label:'Yellow Belt', panelColor: Colors.yellow.shade600,textColor: Colors.grey.shade800, link: 'yellow',),
          StatusPanel(label:'Orange Belt', panelColor: Colors.orange.shade800,textColor: Colors.white, link: 'orange',),
          StatusPanel(label:'Purple Belt', panelColor: Colors.purple.shade700,textColor: Colors.white, link: 'purple',),
          StatusPanel(label:'Green Belt', panelColor: Colors.teal.shade700,textColor: Colors.white, link: 'green',),
          StatusPanel(label:'Brown Belt', panelColor: Colors.brown.shade800,textColor: Colors.white, link: 'brown',),
        ],
      )
    );
  }
}

class StatusPanel extends StatelessWidget {
  final String label;
  final Color panelColor;
  final Color textColor;
  final String link;

  const StatusPanel({Key? key, required this.label, required this.panelColor, required this.textColor, required this.link}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return  GestureDetector(
      onTap: () {
        context.pushNamed(link);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          color: panelColor,
        ),
        height: 100,
        width: width/2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              label,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: textColor
              ),
            ),
          ],
        ),
      ),
    );
  }
}

