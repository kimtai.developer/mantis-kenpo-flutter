import 'package:flutter/material.dart';

class Alert extends StatelessWidget {
  final String label;
  final Color font;
  final Color bg;

  const Alert({Key? key, required this.label, required this.font, required this.bg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      height: size.height * 0.12,
      padding: EdgeInsets.symmetric(horizontal: 10),
      color: bg,
      child: Text(
          label,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w700,
          color: font
        )
      )
    );
  }
}
