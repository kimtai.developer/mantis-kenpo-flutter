import 'package:flutter/material.dart';
import 'package:kenpo/theme/theme.dart';

class CustomTechniques extends StatelessWidget {
  final List techniques;
  final List techniqueDesc;

  const CustomTechniques({Key? key, required this.techniques, required this.techniqueDesc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: techniques.length,
      itemBuilder: (context,index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 4),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: ExpansionTile(
              collapsedShape: const RoundedRectangleBorder(
                side: BorderSide.none,
              ),
              shape: const RoundedRectangleBorder(
                side: BorderSide.none,
              ),
              childrenPadding: const EdgeInsets.symmetric(horizontal: 0,vertical: 5),
              collapsedBackgroundColor: Colors.grey[50],
              collapsedTextColor: const Color(0xff202c3a),
              backgroundColor: Colors.white,
              title: Text(
                  techniques[index],
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: CustomData.fontColor
                  )
              ),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0,vertical: 10),
                  child: Text(
                    techniqueDesc[index] ?? '',
                    textAlign: TextAlign.left,
                    softWrap: true,
                    style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.black
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}