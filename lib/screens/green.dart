import 'package:flutter/material.dart';
import 'package:kenpo/components/index.dart';
import 'package:kenpo/utility/index.dart';
import 'package:kenpo/theme/theme.dart';

class Green extends StatelessWidget {
  const Green({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomData.bodyColor,
      appBar: AppBar(
          backgroundColor: Colors.teal.shade800,
          title: const Text(
            'GREEN BELT',
            style: TextStyle( fontWeight: FontWeight.bold),
          )
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Alert(
              label: 'Level 4, minimum training period should be not less than 1 year attaining Green belt. At least 2 tournament red strip on the green level.',
              font: Colors.white,
              bg: Colors.teal.shade600,
            ),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 1 - Techniques', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomTechniques(techniques: Datasource.greenTechniques,techniqueDesc: Datasource.greenTechniqueDesc),

            const SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
