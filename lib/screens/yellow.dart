import 'package:flutter/material.dart';
import 'package:kenpo/components/index.dart';
import 'package:kenpo/utility/index.dart';
import 'package:kenpo/theme/theme.dart';

class Yellow extends StatelessWidget {
  const Yellow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomData.bodyColor,
      appBar: AppBar(
          backgroundColor: Colors.yellow.shade800,
          title: const Text(
            'YELLOW BELT',
            style: TextStyle( fontWeight: FontWeight.bold),
          )
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
             Alert(
              label: 'Level 1, minimum training period should be not less than 6 months attaining Yellow belt. At least one tournament red strip on the yellow level',
              font: Colors.grey.shade800,
              bg: Colors.yellow,
            ),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 1', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomExpansion(title: ' Advanced Arm Basic Attacks',arg: Datasource.advanceArmBasic),
            const SizedBox(height: 4),
            CustomExpansion(title: ' Basic Joint Locks',arg: Datasource.basicJointLocks),
            const SizedBox(height: 4),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 2 - Techniques', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomTechniques(techniques: Datasource.yellowTechniques,techniqueDesc: Datasource.yellowTechniqueDesc),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 5),
              child: Text('Part 3', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomExpansion(title: 'Basic Takedowns',arg: Datasource.takeDowns),
            const SizedBox(height: 4),
            CustomExpansion(title: 'Basic Counters',arg: Datasource.basicCounters),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Basic Form One', style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}

