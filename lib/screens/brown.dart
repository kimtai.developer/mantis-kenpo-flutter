import 'package:flutter/material.dart';
import 'package:kenpo/components/index.dart';
import 'package:kenpo/utility/index.dart';
import 'package:kenpo/theme/theme.dart';

class Brown extends StatelessWidget {
  const Brown({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomData.bodyColor,
      appBar: AppBar(
          backgroundColor: Colors.brown.shade900,
          title: const Text(
            'BROWN BELT',
            style: TextStyle( fontWeight: FontWeight.bold),
          )
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Alert(
              label: 'Level 5, minimum training period should be not less than 2 years attaining brown belt. At least 2 tournament red strip on the brown level.',
              font: Colors.white,
              bg: Colors.brown.shade600,
            ),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 1 - Techniques', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomTechniques(techniques: Datasource.brownTechniques,techniqueDesc: Datasource.brownTechniqueDesc),

            const SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
