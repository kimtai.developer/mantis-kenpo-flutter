import 'package:flutter/material.dart';
import 'package:kenpo/components/index.dart';
import 'package:kenpo/theme/theme.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomData.bodyColor,
      appBar: AppBar(
          centerTitle: true,
        title: const Text(
          'KENPO KARATE',
          style: TextStyle( fontWeight: FontWeight.bold),
        )
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              color: Colors.blue.shade900,
              height: 90,
              padding: const EdgeInsets.all(10),
              child: const Text(
                'Africa Kenpo Federation, developing martial arts skills for modern self defence and sports',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.white
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 12,vertical: 7),
              child: Text(
                'Level Belts',
                style: TextStyle( fontWeight: FontWeight.bold,fontSize: 16),
              ),
            ),

            const Levels1(),

            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 7),
              child: Text(
                'N.B',
                style: TextStyle( fontWeight: FontWeight.bold,fontSize: 20),
              ),
            ),
            const BulletList(strings :[
              'The techniques in each level should not be trained by a non A.K.F member.',
              'Clean and in good condition uniform with a belt is a must for all members.',
              'All techniques must be applied in line with basic requirements.',
              'Always KI after last movements on the set.',
              'Coordination is of essence',
            ]),
          ],
        ),
      )
    );
  }
}
