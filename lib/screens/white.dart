import 'package:flutter/material.dart';
import 'package:kenpo/components/index.dart';
import 'package:kenpo/utility/index.dart';
import 'package:kenpo/theme/theme.dart';

class White extends StatelessWidget {
  const White({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomData.bodyColor,
      appBar: AppBar(
        title: const Text(
          'WHITE BELT',
          style: TextStyle( fontWeight: FontWeight.bold),
        )
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Alert(
              label: 'Initiate level, minimum training period should be not less than 3 months from the date of registration.',
              font: Colors.grey.shade900,
              bg: Colors.white,
            ),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 1 -  Basic Moves', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomExpansion(title: 'Closed Hand Movements',arg: Datasource.closehand),
            SizedBox(height: 2,),
            CustomExpansion(title: 'Open Hand Movements',arg: Datasource.openhand),
            SizedBox(height: 2,),
            CustomExpansion(title: 'Blocks',arg: Datasource.blocks),
            SizedBox(height: 2,),
            CustomExpansion(title: 'Leg Moves',arg: Datasource.legmoves),
            SizedBox(height: 2,),
            CustomExpansion(title: 'Kicks',arg: Datasource.kicks),
            SizedBox(height: 2,),
            CustomExpansion(title: 'Stances',arg: Datasource.stances),
            SizedBox(height: 2,),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 2 - Applied Basics', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomExpansion(title: 'Apllied Basics',arg: Datasource.appliedbasics),
            SizedBox(height: 2,),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 3 - Basic Combination', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomExpansion(title: 'Basic Combination',arg: Datasource.basicCobmination),
            SizedBox(height: 2,),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 4 - Punching Drill', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomExpansion(title: 'Punching Drill',arg: Datasource.punchingDrill),
            SizedBox(height: 50,),

          ],
        ),
      )
    );
  }
}









