import 'package:flutter/material.dart';
import 'package:kenpo/components/index.dart';
import 'package:kenpo/utility/index.dart';
import 'package:kenpo/theme/theme.dart';

class Orange extends StatelessWidget {
  const Orange({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomData.bodyColor,
      appBar: AppBar(
          backgroundColor: Colors.orange.shade900,
          title: const Text(
            'ORANGE BELT',
            style: TextStyle( fontWeight: FontWeight.bold),
          )
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Alert(
              label: 'Level 2, minimum training period should be not less than 6 months attaining Orange belt. At least one tournament red strip on the orange level.',
              font: Colors.white,
              bg: Colors.orange.shade700,
            ),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 1 - Techniques', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomTechniques(techniques: Datasource.orangeTechniques,techniqueDesc: Datasource.orangeTechniqueDesc),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 2', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomExpansion(title: 'Advance Takedowns',arg: Datasource.advanceTakedowns),
            const SizedBox(height: 4),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 3', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomExpansion(title: 'Basic defense from grabs ',arg: Datasource.basicDefenceFromGrabs),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Basic Form Two', style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
