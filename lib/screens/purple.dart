import 'package:flutter/material.dart';
import 'package:kenpo/theme/theme.dart';
import 'package:kenpo/components/index.dart';
import 'package:kenpo/utility/index.dart';

class Purple extends StatelessWidget {

  const Purple({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Size size =  MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: CustomData.bodyColor,
      appBar: AppBar(
          backgroundColor: Colors.purple.shade800,
          title: const Text(
            'PURPLE BELT',
            style: TextStyle( fontWeight: FontWeight.bold),
          )
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Alert(
              label: 'Level 3, minimum training period should be not less than 9 months attaining Purple belt. At least one tournament red strip on the purple level.',
              font: Colors.white,
              bg: Colors.purple,
            ),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Part 1 - Techniques', style: TextStyle(fontWeight: FontWeight.w700,fontSize: 16),),
            ),
            CustomTechniques(techniques: Datasource.purpleTechniques,techniqueDesc: Datasource.purpleTechniqueDesc),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 15,vertical: 10),
              child: Text('Series One', style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}

