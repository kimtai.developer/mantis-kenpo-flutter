import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kenpo/screens/index.dart';
import 'package:kenpo/theme/theme.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'l10n/l10n.dart';

void main() {
  final GoRouter _router = GoRouter(
    routes: [
        GoRoute(
          name: "home",
          path: "/",
          builder: (context, state) => const Home(),
        ),
        GoRoute(
          name: "white",
          path: "/white",
          builder: (context, state) => const White(),
        ),
        GoRoute(
          name: "yellow",
          path: "/yellow",
          builder: (context, state) => const Yellow(),
        ),
        GoRoute(
          name: "orange",
          path: "/orange",
          builder: (context, state) => const Orange(),
        ),
        GoRoute(
          name: "purple",
          path: "/purple",
          builder: (context, state) => const Purple(),
        ),
        GoRoute(
          name: "green",
          path: "/green",
          builder: (context, state) => const Green(),
        ),
        GoRoute(
          name: "brown",
          path: "/brown",
          builder: (context, state) => const Brown(),
        ),
      ],
  );
  runApp(MaterialApp.router(
    debugShowCheckedModeBanner: false,
    routerConfig: _router,
    theme: CustomData.lightTheme,
    supportedLocales: L10n.all,
    localizationsDelegates: const [
      AppLocalizations.delegate,
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
    ],
  ));
}


