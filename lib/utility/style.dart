import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.white,
  primary: Colors.pink[800],
  minimumSize: const Size.fromHeight(50),
  elevation: 0,
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(5)),
  ),
);

TextStyle textStyle = GoogleFonts.robotoCondensed(
    fontSize: 16,
    color: Colors.white
);

TextStyle textStyle1 = GoogleFonts.robotoCondensed(
  fontSize: 16,
);

TextStyle counterStyle = GoogleFonts.kalam(
    fontSize: 50,
    fontWeight: FontWeight.w700,
    color: Colors.blue[900]
);