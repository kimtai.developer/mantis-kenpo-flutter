import 'package:flutter/material.dart';

class Datasource {
  static List closehand = ["Front Punch","Reverse Punch","Back Knuckle (front & reverse)", "Hammer Fist (front & reverse)"];
  static List openhand = ["Check","Chop","Finger Poke (front & reverse)","Ridge hand"];
  static List legmoves = ["Step drags (forward & Backwards)","Step through n step back","Crossover","Spinning"];
  static List blocks = ["Upward block","Downward block","Inward block","Outward block","Inside parry","Outside parry"];
  static List kicks = ["Ball kick (front & rear)","Roundhouse kick (low, high front & rear)","Side kick (front & rear)","Back kick","Cresent kick (inward & outward)"];
  static List stances = ["Fighting stance","Horse stance","Back stance","Cat stance","Nuetral stance","Nuetral bow"];
  static List appliedbasics = [
    "The initiator should perform the above basic strike with an opponent",
    "All moves must be directed to the intended target",
    "The initiate should maintain proper basics at all times"
  ];
  static List basicCobmination = [
    "From fighting stance step through upward block, reverse punch, ballkick",
    "From fighting stance step through inward block, spinning back knuckle, front sidekick",
    "From fighting stance step drag back, downward block, reverse finger poke, front roundhouse kick",
    "From fighting stance step drag back, outward block,reverse punch, spinning sidekick",
  ];
  static List punchingDrill = [
    "Front punch, reverse punch, step through, reverse punch - 3 times only turn",
    "Front punch, reverse punch, crossover, reverse punch - 3 times turn repeat",
    "Front punch, front punch, step drag,reverse punch - 3 times turn repeat",
    "Reverse punch, rare ridge hand with a step through - 3 time only turn",
    "Check, chop, finger poke, hammer fist to groin with a crossover, back fist, reverse punch - 3 times turn repeat"
  ];
  static List advanceArmBasic = [
    "Types of finger pokes: 1,2,3,4",
    "Finger flick","Chops","Ridge hand strikes",
    "Hammer fists","Finger grips: 1,2,3,4",
    "Cranks: neck, jaws, eyes, ears","Elbow strikes (4 types)",
    "Chicken head strike","Rakes","Palm strikes"
  ];
  static List basicJointLocks = ["Elbow","Wrist","Finger","Shoulder","Knee","Ankle","Toes"];
  static List yellowTechniques = [
    "Striking Wings (Step through (R) punch to head)",
    "Sweeping the Log (Step through (R) punch to head)",
    "Descending Sword (Step through (L) punch to the head)",
    "Dropping Heel (Right wild punch)"
  ];
  static List yellowTechniqueDesc = [
    "Right leg side step to 2 O'clock with a (L) cover block, (R) reverse punch to the solar plexus, (L) chop to the neck, (L) ball kick to the groin",
    "Left leg side step to 10 O'clock, (L) inside parry, (R) under punch to the ribs, (L) cross forearm strike to the opp's face, counter grab opp's right shoulder, (L) leg boot to boot sweep, (R) dropping reverse punch",
    "Side step to 11 O'clock with (L) leg, (L) inside (R) outside parry, (L) chop to the neck, (R) reverse punch to the solar plexus, (L) ball kick to the groin, step (L) leg to 9 O'clock, (R) dropping forearm strike to the base of neck",
    "Duck under to the left with a (R) reverse punch to the solar plexus, grap opp's shoulder, rare leg (R) take down, (R) foot stomp to any available target, step over the opponent, cross out",
  ];
  static List takeDowns = ["Boot to boot","Rear leg","Chok and drop","Twist and torque","Reverse leg trip"];
  static List basicCounters = ["Front punch","Reverse punch","Back Kick","Roundhouse kick","Side kick"];
  static List orangeTechniques = [
    "Slicing Sword (right punch to the face)",
    "Fatal Steel (right back kick)",
    "Flicking Mantis (right punch to the face)",
    "Twist of Fate (right shoulder grab)",
    "Attacking Wing (two arm grab of left arm above elbow)",
    "Hooking the Mule (rear leg roundhouse)"
  ];
  static List orangeTechniqueDesc = [
    "Step your (L) footback, (R) hand check to opp's neck, step(R) leg to 1 O'clock @ (L) snapping hammer fist to opp's groin, (L) grab opp's left hand, (R) reverse punch to the kidney, (R) shin kick to opp's leg, cross back, step back, fighting stance.",
    "Step your (R) foot 1 O'clock @left downward block, (R) back knuckle to opp's face, (L) reverse punch to solar plexus, (R) swinging hook punch to opp's lower jaw, (R) side kick to any available target.",
    "Step  your (L) foot back, (L) inside/(R) outside parry, quickly drop your (R) hand down & finger flick to opp's groin, quickly grap opp's shoulder, push opp back while stepping through with (L) leg, (R) right over punch to his face, (R) foot scope to groin, fighting stance.",
    "Step  your (L) foot back,(L) hand counter grab opp's (R) hand, finger pork, swing into (R) armpit trap, (L) 12 O'clock @left reverse hammer strike to groin, raising (L) elbow strike to face, step (L) leg to 6 O'clock, raise opp's (R) hand, side kick to opp's mid section, cross back, fighting stance.",
    "Take a big step forward with (R) leg, three finger grip of both hands, raise your (R) knee & hands high, step behind @snatch both hands to your (L) hip, (R) elbow to the face (R) push kick opp away, fighting stance.",
    "(L) hand hook & trap opp's (R) leg, (R) sidekick to the knee, (R) finger poke to eyes, two hand ankle lock, force opp down, (L) outside axe kick to the opp's head"
  ];
  static List advanceTakedowns = [
    "Hip throw","Shoulder throw","Dragon tails sweep","Front tackle: front & back",
    "Bear hugs: free hands & hands tied","Break falls: forward,backward,side","Rolls: forward & backward"
  ];
  static List basicDefenceFromGrabs = ["Against Guillotine choke defense","Against front bear hug","Against rear bear hug"];
  static List purpleTechniques = [
    "Capturing Wings (strong two hand push to the chest)", "'Nyonga nyoka' Choking the snake (right ST reverse punch)",
    "'Tongua dhoruba' Reversing the storm (right shoulder grab and right punch to the face)","Hugging the bear (quick left & right punch to the head)",
    "Defending prey (hand gun pointed at the chest)","Bringing down to the bear (left hand grab and right punch to the face)",
    "Attacking Snake (left hand choke and right arm bar,right punch to the face","Revenging Snake (escape from the arm bar)",
    "Lock Horns (right side head lock)", "Attacking the branch (roundhouse kick to the face)",
    "Grasp of destruction (wild punch to the head)","Crushing branches (two arm grab at the chest)"
  ];
  static List purpleTechniqueDesc = [
    "Stand straight as your opponent tries to push you to the ground, as his hands hit your chest you reach in as far as you can and counter grab his arms above his elbows, drop your (R) foot back and strike him with a palm strike to the nose and (R) snapping foot scoop to the groin, (R) forearm to his forehead and lift him backup, you ar still holding his arm (R) arm, now swing into armpit trap, continue to swing around with a left elbow to the side of the face, continue to swing around and apply a reverse headlock, a powerful knee to the body, two hand neck break as you step back.",
    "As Punch comes, step your (L) leg back and counter with (L) inside parry & (R) outside parry, strike his groin with a quick roundhouse snapping kick, using the same leg to do a sidekick to the back of the opp's leg, as he goes down to the knee hit his kidney with a high dropping (L) reverse punch, apply chock and drop to take him off balance.",
    "Step your (R) foot back, use your (L) arm to stop his punch at the bicep, at the same time strike his nose with an upper (R) palm, your hand doesn't stop, it swings around his (L) arm into a reverse armpit trap, now do a rear leg takedown with your (L) leg with (L) ridge hand to the chest, before he touches the ground pull up on his arm and break it.",
    "Use the cover blocks to block his punches, quickly jump to him and grab him chest to chest, put your (R) arm above the opp's (R) shoulder (L) arm under is (R) arm, now quickly bend him forward with a shoulder lock his (R) arm, use both arms to apply the shoulder lock, as he bend over you knee him to the rib cage, twist left while still holding his (R) arm and a reverse heel kick to his face, don't let go his (R) arm, continue to twist right and strike his spine with a dropping elbow release his arm and hip check him away.",
    "You must move very quickly or you will be shot!, First use your (L) hand to slap the gun from your face and then trap it in your (R) hand before he can shoot, don't move your head when grabbing the gun, now step back as you put him off balance using a wrist lock, make sure the gun never faces you, as he moves forward you quickly strike him on the head with his own gun the apply a rear leg takedown, as he is going you are still holding the gun with two hands, twist the gun so it is pointing at his face, now step on his chest as you wrist lock pulling the gun from him, point it to him.",
    "(L) hand pal to opp's (R) bicep, counter his (L) elbow, head butt, jumping (L) knee to the body counter grab (R) arm, shoulder throw takedown, hold the arm, (R) storm to the groin, (R) heel kick to the face, cross over and step through.",
    "Block with (L) hand a opp's (L) arm, (R) Cross kick to opp's (L) arm, (R) cross kick to side of opp's (L) leg, storm toe as you break free, turn with (L) extended block (R) dropping reverse punch to groin, (R) upper finger poke to eyes (R) leg fade away sidekick to the opp's leg.",
    "From the locked position grab opp's ankle and bite, legs folded, counter grab your (R) arm and pull, finger poke to eyes, push him away and roll to left and stand on your feet, (L) fake punch to the head, cross over, (L) quick kick to the groin, (R) foot stomp on the opp's knee, (L) roundhouse kick to the face.",
    "Step (R) foot between the opp's feet (R) ridge hand to the groin with a (L) hammer fist to the kidney, (L) hand pushes face under nose as you straighten your back, (R) hammer fist strike to opp's solar plexus, grab opp's arm from behind your neck and bring him forward with a forearm strike, (L) elbow at the spine, (L) reverse foot scoop  to the groin pull (R) away, takedown, thrown opponent, palm smash to the back of the head, cross back and step through.",
    "Reverse (R) foot scoop to the groin, arm grab and snatch, neck grab with (R) knee to the face, (R) palm to the chest (L) with a (R) foot scoop, (L) crescent kick to the head.",
    "Step you (L) leg back (L&R) check (R) finger poke (R) hand grab with (R) thrusting knee to solar plexus, Step (R) leg outside to 4 O'clock (L) dropping reverse punch, step (R) leg at 6 O'clock (L) fade away sidekick to knee.",
    "Place (R) hand over and (L) hand over the opp's (R) shoulder blade, pull opponent close to you (R) short snapping elbow to the face, (R) knee to the groin, force opp to bend over with a reverse shoulder lock, (R) knee to the face, (R) rear leg takedown, (L) arm hold opp ,(R) double wrist lock, apply the crusher (one knee to the neck and other to the ribs).",
  ];
  static List greenTechniques = [
    "Branch of Obscurity (Wild right punch to face)","Grasping Mantis (left & right punches to the face)",
    "Locking the branch (Wild right punch to face)","Destructive Storm (Wild right club attack)",
    "Double sword (Wild right punch)","Defending branches (Two hands reaching cut)","Invisible Fist (Offensive attack: you strike first)"
  ];
  static List greenTechniqueDesc = [
    "Step offline to your left while ducking under his wild punch, attack his groin with an upward right reverse punch as you step drag to your left, as he bends down holding his groin hit his face with a powerful right reverse punch, bent knee back kick to the face with the side of your foot, step to his side and break his knee with a strong sidekick to the side of his knee, the run away. (Tell your opp's to grab his groin immediately, you hit. The punch and kick must be done as one movement. The sidekick is a jumping sidekick. Don't forget to run).",
    "As the opponent swings the left punch. You duck under it and move outside to your right, now the opponent swings a wild punch, you move back inside with the left extended outward block that stops his punch, grab his hand, right upper cut to the chin, right downward ridge hand to opp's arm and twist to his side, apply wristlock, overhead two arm choke, take opponent off balance and choke. (As you strike ridge hand step to his side and force him to lean back with the wrist lock. You must move quickly when taking his hand over his head for the choke)",
    "Duck under punch moving to your left, right quick to groin, opp holds his groin with left hand and reaches for you with his right hand, slap his hand away, back knuckle to his face, move inside grabbing his right leg up pushing forward & sweep takedown, ankle lock, opp kicks at you with his rightleg, slap leg away & release ankle, step your right leg to opp's left side & dropping left knee to groin, opp sits up and grab groin, move behind apply choke, sit down & lean back.",
    "Block and counter grab opp's wrist, Right finger flick to eyes, pull right arm and apply an armpit trap, left hammer fist to groin and elbow to his chin, counter grab, slap his trapped right hand & apply wrist lock with your right hand, step out with your left foot then turn back with a high downward hammer fist to opp's right arm at the elbow, apply left armpit trap as you switch to a left hand counter grab and wrist lock of opp's right hand, use your right leg and do a rear leg takedown, drop your right knee to this stomach as you quickly strike with a right reverse punch.",
    "Step your left foot backand and block with right inward block, after the block slide your right arm into the side of his neck with a snap do not step in, now quickly step your right foot out slightly and attack his body with a snap do not step in, now quickly step your right foot out slightly and attack his body with a powerful left punch to his solar plexus, twist back with a left upper cut to his chin while bringing your left hand to protect your face, after you strike the chin quickly attack his throat with a left hand chop, as he grabs his throat kick him in the groin and then in the head with a crescent kick with your same left leg.",
    "With hands at shoulder level in front of him, your opponent now walks towards you without stopping, you must lean back and kick his hands with a right fade away sidekick, then cross the leg back, turn and kick his hands again with a left leg fade away kick. This technique shows the instructor that you can maintain balance while kicking and moving backwards. (You must end in a strong fighting stance for it to be done correctly).",
    "You and your opponent are in a fighting stance with the leg back, attack him with a right hand check to his lead hand then quickly punch his face with a back knuckle, reverse punch to his stomach and hook punch to his ribs, as he slowly steps back left finger flick him in the eys as you start to crossover, dropdown with a right hammer fist to his groin, as you come up strike is head up with a left palm strike, quick back fist to his eye before you finish crossing over in a fighting stance, jumping sidekick to push him away."
  ];
  static List brownTechniques = [
    "Imposing Force (Wild right punch to face)","Hammering Mantis (2 hand push to the chest, Right leg roundhouse)",
    "Twin strike to the mantis (Attempted two arm tackle)","Double Trouble( Straight back fist to the face,two arm grab from behind arms)",
    "Sword of the mantis (Right Surprise punch to the head) Modified"
  ];
  static List brownTechniqueDesc = [
    "Step the (R) leg back, (L) extended outward block, (R) back fist to the face, step through with the (R) leg, (L) reverse punch to solar plexus, (R) upward palm to the chin, (R) front ball kick to the groin, body trap + reverse headlock chock & release, elbow mash to the temple, knee break.",
    "Step the (R) leg back grab opp's (L) arm with your (R) hand, (L) finger poke, counter grab opp's (L) wrist (L) hand, S.T with your (R) leg (R) forearms force down, two hammering (R) forearms to opp's arm, (R) punch to the mastoid bone, (R) knee to the chest, push away. 2. (L) spinning back kick to the groin, (R) cross punch to the face, (R) spinning kick to the chest.",
    "Double palm strike to ears, head butt, double knuckle strike to the ribs, (L) arm grab back of head, (R) elbow smash, (R) inside thigh kick to the side of the knee.",
    "(R) spinning forearm to the head, (L) Ti-kick to the back of the opp's lead leg, (R) side across the face @(L)(R) elbow to the head, (R) hand tight grip to the groin torque a round front ballkick to the solar plexus, spinning drop kick to the side of leg, grab shoulders and lift, grab head, neck break.",
    "Pivot the right (L) cover block, pivot to the left, (R) uppercut to the chin, (R) ball kick to the groin, (R) palm slap to the opp's left ear, move inside grabbing opp's (R) arm, armpit trap, (L) hammer fist to the groin, (L) fake hammer fist, (L) elbow to the head, arm lock."
  ];
}
