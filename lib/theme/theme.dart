import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomData {
  static Color fontColor = const Color(0xff05070A);
  static Color bodyColor = const Color(0xffe8e9ea);
  static ThemeData lightTheme = ThemeData(
    primarySwatch: Colors.blue,
    primaryColor: Color(0xff202c3a),
    fontFamily: 'Titillium Web',
    textTheme: GoogleFonts.titilliumWebTextTheme(),
    scaffoldBackgroundColor: Color(0xff1a212a),
    appBarTheme: const AppBarTheme(
      backgroundColor: Color(0xff1a212a),
      foregroundColor: Colors.white,
      elevation: 0,
    )

  );
  static ThemeData whiteBelt = ThemeData(
    appBarTheme: const AppBarTheme(
      foregroundColor: Colors.white,
      elevation: 0,
    )
  );
  static ThemeData yellowBelt = ThemeData(
    appBarTheme: const AppBarTheme(
      foregroundColor: Color(0xffF4B81C),
      elevation: 0,
    )
  );
  static ThemeData orangeBelt = ThemeData(
    appBarTheme: const AppBarTheme(
      foregroundColor: Color(0xffF7941E),
      elevation: 0,
    )
  );
  static ThemeData purpleBelt = ThemeData(
    appBarTheme: const AppBarTheme(
      foregroundColor: Colors.purple,
      elevation: 0,
    )
  );
  static ThemeData greenBelt = ThemeData(
    appBarTheme: const AppBarTheme(
      foregroundColor: Colors.teal,
      elevation: 0,
    )
  );
  static ThemeData brownBelt = ThemeData(
    appBarTheme: const AppBarTheme(
      foregroundColor: Colors.brown,
      elevation: 0,
    )
  );


}